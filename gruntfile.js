module.exports = function(grunt) {
  // REQUIRE

  // measure the time
  require('time-grunt')(grunt);
  // require serve-static
  var serveStatic = require('serve-static');
  // load the tasks
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin'
  });

  var config = {
    // Minify the scripts and css for production

  };

  // Development Url for Browser sync proxy
  var devUrl = "craft.dev";

  // CONFIG
  grunt.initConfig({
    config: config,
    pkg: grunt.file.readJSON('package.json'),

    // LibSass
    sass: {
      dev: {
        options: {
          // For some reason, nested breaks source map functionality
          outputStyle: 'expanded',
          sourceMap: true
        },
        files: [{
          expand: true,
          cwd: 'src/css',
          src: '*.scss',
          dest: 'app/public/dist/assets/css',
          ext: '.css'
        }]
      },
      prod: {
        options: {
          outputStyle: 'compressed',
          sourceMap: false
        },
        files: [{
          expand: true,
          cwd: 'src/css',
          src: '*.scss',
          dest: 'app/public/dist/assets/css',
          ext: '.css'
        }]
      }
    },

    modernizr: {
      dev: {
        options: [
          "setClasses"
        ],
        dest: 'app/public/dist/assets/js/libs/modernizr-custom.js',
        extra: {
          'shiv' :       false,
          'printshiv' :  false,
          'load' :       true,
          'mq' :         false,
          'cssclasses' : true
        },
        extensibility: {
          'addtest':      true,
          'prefixed':     false,
          'teststyles':   false,
          'testprops':    false,
          'testallprops': false,
          'hasevents':    false,
          'prefixes':     false,
          'domprefixes':  false
        },
        crawl: false,
        excludeTests: ['hidden'],
        tests: ( function() {
          var tests = grunt.file.readJSON('./node_modules/modernizr/lib/config-all.json')['feature-detects'];
          // Return first-level tests
          return tests.map(function(str) {
            return str.replace('-','');
          }).concat([
            // Additional tests
            'inlinesvg'
          ]);
        }() )
      },
      prod: {
        cache: true,
        options: [
          "setClasses"
        ],
        dest: 'app/public/dist/assets/js/libs/modernizr-custom.js',
        extra: {
          'shiv' :       false,
          'printshiv' :  false,
          'load' :       true,
          'mq' :         false,
          'cssclasses' : true
        },
        extensibility: {
          'addtest':      true,
          'prefixed':     false,
          'teststyles':   false,
          'testprops':    false,
          'testallprops': false,
          'hasevents':    false,
          'prefixes':     false,
          'domprefixes':  false
        },
        files: {
          src: [
            'app/public/dist/assets/js/{,*/}*.js',
            'app/public/dist/assets/css/{,*/}*.css'
          ]
        },
        uglify: true,
        excludeTests: ['hidden'],
        tests: [
          'pointerevents',
          'touchevents',
          'inlinesvg'
        ]
      }
    },

    postcss: {
      options: {
        processors: [
          require('autoprefixer')()
        ]
      },
      dev: {
        /*options: {
         map: true
         },*/
        /*expand: true,
         cwd:  'dist',*/
        src:  'app/public/dist/assets/css/main.css',
        dest: 'app/public/dist/assets/css/main.css'
      },
      prod: {
        expand: true,
        cwd:  'dist',
        src:  'app/public/dist/assets/css/main.css',
        dest: 'app/public/dist/assets/css/main.css'
      }
    },

    browserify: {
      dist: {
        options: {
          transform: ['babelify']
        },
        files: [{
          expand: true,
          flatten: true,
          cwd:  'src/js',
          src:  '*.js',
          dest: 'app/public/dist/assets/js'
        }]
      }
    },

    svgstore: {
      options: {
        prefix : 'icon-', // This will prefix each ID
        svg: { // will add and overide the the default xmlns="http://www.w3.org/2000/svg" attribute to the resulting SVG
          viewBox : '0 0 100 100',
          xmlns: 'http://www.w3.org/2000/svg',
          style: 'display: none'
        }
      },
      default: {
        files: {
          'src/templates/includes/_icons.html': ['src/svg/*.svg'],
        }
      },
    },

    // Automatic Bower script and styles inclusion
    wiredep: {
      main: {
        exclude: [
          'bower_components/modernizr/modernizr.js',
          'bower_components/animate.css/animate.css'
        ],
        // Point to the files that should be updated when
        // you run `grunt wiredep`
        src: [
          'src/templates/includes/**/*.html'
          // 'src/css/main.scss'
        ],
        options: {
          // See wiredep's configuration documentation for the options
          // you may pass:

          // https://github.com/taptapship/wiredep#configuration
        }
      }
    },

    eslint: {
      target: ['src/js/main.js']
    },

    concat: {
      options: {
        separator: ';'
      }
    },

    clean: {
      dist: [ 'app/public/dist/' ],
      tmp:  [ '.tmp/ '],
      unminified: ( function () {
        if ( config.minifyScripts ) {
          return [
            'app/public/dist/assets/css/main.css',
            'app/public/dist/assets/js/main.js'
          ];
        } else {
          // Scripts are not minified, nothing to clean
          return [];
        }
      }() ),
      unrevved: [
        'app/public/dist/assets/css/main.min.css',
        'app/public/dist/assets/js/main.min.js'
      ]
    },

    imagemin: {
      prod: {
        files: [{
          expand: true,
          cwd: 'src/img/',
          src: ['**/*.{png,svg,jpg,gif}'],
          dest: 'app/public/dist/assets/img'
        }]
      }
    },

    copy: {
      fonts: {
        expand: true,
        cwd: 'src',
        src: 'font/**',
        dest: 'app/public/dist/assets/'
      },
      templates: {
        expand: true,
        cwd: 'src',
        src: 'templates/**',
        dest: 'app/craft/'
      },
      favicons: {
        expand: true,
        cwd: 'src',
        src: 'favicons/**',
        dest: 'app/public'
      },
      config: {
        expand: true,
        cwd: 'src',
        src: 'config/**',
        dest: 'app/craft/'
      },
      plugins: {
        expand: true,
        cwd: 'src',
        src: 'plugins/**',
        dest: 'app/craft/'
      },
      images: {
        expand: true,
        cwd: 'src',
        src: 'img/**',
        dest: 'app/public/dist/assets/'
      },
      video: {
        expand: true,
        cwd: 'src',
        src: 'video/**',
        dest: 'app/public/dist/assets/'
      },
      unminifiedStyles: {
        expand: true,
        cwd: '.tmp/concat/assets/css',
        src: '**/*',
        dest: 'app/public/dist/assets/css'
      },
      unminifiedScripts: {
        expand: true,
        cwd: '.tmp/concat/assets/js',
        src: '**/*',
        dest: 'app/public/dist/assets/js'
      }
    },

    eol: {
      to_lf_replace: {
        options: {
          eol: 'lf',
          replace: true
        },
        files: {
          src: [
            'src/templates/*.html'
          ]
        }
      }
    },

    /*favicons: {
     options: {
     tileBlackWhite: false,
     html: 'src/templates/includes/_favicons.html'
     },
     icons: {
     src: 'src/misc/favicon.png',
     dest: 'app/public/'
     }
     },*/

    concurrent: {
      options: {
        limit: 100
      },
      prod: [
        'sass:prod',
        'browserify',
        'imagemin:prod'
      ]
    },

    watch: {
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep']
      },
      scripts: {
        files: ['src/js/main.js'],
        tasks: ['eslint', 'browserify']
      },
      fonts: {
        files: ['src/fonts/**'],
        tasks: ['copy:fonts']
      },
      styles: {
        files: ['src/css/**'],
        tasks: ['sass:dev', 'postcss:dev']
      },
      images: {
        files: ['src/img/**'],
        tasks: ['newer:copy:images']
      },
      video: {
        files: ['src/video/**'],
        tasks: ['copy:video']
      },
      templates: {
        files: ['src/templates/**/*.html'],
        tasks: ['copy:templates']
      },
      favicons: {
        files: ['src/favicons/**'],
        tasks: ['copy:favicons']
      },
      config: {
        files: ['src/config/**/*'],
        tasks: ['copy:config']
      },
      plugins: {
        files: ['src/plugins/**/*'],
        tasks: ['copy:plugins']
      },
      svg: {
        files: ['src/svg/**'],
        tasks: ['svgstore']
      }
    },

    uglify : {
      dist : {
        src : 'app/public/dist/assets/js/main.js',
        dest : 'app/public/dist/assets/js/main.js'
      }
    },

    browserSync: {
      dev: {
        bsFiles: {
          src : [
            "app/craft/templates/**/*.html",
            "app/craft/plugins/**/*.php",
            "app/public/dist/assets/css/main.css",
            "app/public/dist/assets/js/main.js",
            "app/public/dist/assets/img/*",
          ]
        },
        options: {
          notify: true,
          watchTask: true, // < VERY important
          proxy: devUrl
        }
      }
    }

  });

  grunt.registerTask('server', [
    'browserSync',
    'watch'
  ]);

  grunt.registerTask('dev', [
    'eslint',
    'clean:tmp',
    'clean:dist',
    'modernizr:dev',
    'sass:dev',
    'postcss:dev',
    'browserify',
    'svgstore',
    /*'favicons',*/
    'eol',
    'wiredep',
    'copy:fonts',
    'copy:images',
    'copy:templates',
    'copy:favicons',
    'copy:video',
    'copy:config',
    'copy:plugins'
  ]);

  grunt.registerTask('prod', [
    'eslint',
    'clean:tmp',
    'clean:dist',
    'svgstore',
    /*'favicons',*/
    'concurrent:prod',
    'postcss:prod',
    'uglify',
    'modernizr:prod',
    'eol',
    'wiredep',
    'copy:fonts',
    'copy:images',
    'copy:video',
    'copy:templates',
    'copy:config',
    'copy:plugins',
  ]);
};