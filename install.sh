#!/bin/bash

##
##  This install script handles everything that Crafty Vagrant needs on the
##  local machine. Puppet handles everything on the Vagrant box.
##

## Colours for prettier output + to distinguish this script's output:
color='\033[1;36m';   # Light cyan
NC='\033[0m';     # No color

## Function to prompt for user response:
prompt () {
    while true; do
      echo;
        read -p "$1" yn
        case $yn in
            [Yy]* ) return 0;;
            [Nn]* ) return 1;;
            * ) echo "Please answer yes or no.";;
        esac
    done
}

## output in colour:
echo_color() {
  echo -e "${color}$1${NC}";
}

if ! prompt "Do you wish to install Crafty Vagrant? [yn] "; then
  echo_color 'Ok. Install cancelled.';
  exit;
fi

## If Craft isn't present:
if [ ! -d "app/craft" ]; then
  ## Install it!
  bash puppet/makeItCraft.sh
fi

## Install node modules:
echo_color "
## npm install";
npm install;

## Install bower__components:
echo_color "
## bower install";
bower install;

## Set up Craft stuff:
echo_color "
## Setting up Craft...";

## Activate the htaccess (rename 'htaccess' --> '.htaccess'):
if [ -f "app/public/htaccess" ]; then
  echo "## Activating htaccess...";
  mv app/public/htaccess app/public/.htaccess;
fi

## Run grunt dev, to do initial compile of CSS, Javascript, etc:
echo_color "
## grunt dev";
grunt dev;

## Create the storage/runtime directory for Craft
## (to prevent a PHP error on first visit)
DIR="app/craft/storage/runtime";
if [ ! -f $DIR/.gitignore ]; then
  mkdir -p $DIR/{cache,compiled_templates,logs};
  touch "$DIR/.gitignore";
  touch "$DIR/logs/craft.log";
fi

echo_color "
## Finished!
##
## 'vagrant up' to start the server.
## 'grunt server' to start browserSync and watch task (Sass + JS auto update changes in browser).
##
## If this is a new install, you'll need to install Craft at http://craft.dev/admin/install";
