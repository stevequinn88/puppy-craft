# Website stating scaffold w/Vagrant

## Pre-requisites
* [npm](https://www.npmjs.com/)
* [Grunt](http://grunt.com/)
* [Vagrant](http://www.vagrantup.com/)
* [Virtual Box](https://www.virtualbox.org/)
* rsync (optional, for asset-syncing)

## Installation

1. Run the command `bash install.sh` (you might need to `sudo` this). This will install and set up everything necessary (including Craft) in your project directory. If this is a fresh Craft install.

2.  Open your `hosts` file and add
	192.168.56.101    craft.dev

  (the hosts file is usually found at `/etc/hosts` on OSX/Linux; `%SystemRoot%\System32\drivers\etc\hosts` on Windows)

3. Run the command vagrant: `vagrant up`

...and hopefully (after a short wait while your Vagrant machine is set up) you should be ready to go! The webserver should now be accessible from `http://craft.dev/`. If Craft backups are present in `/app/craft/storage/backups`, the most recent one will automatically have been used to populate the database. Otherwise, you can install Craft by going to [http://craft.dev/admin/install](http://craft.dev/admin/install)

## Usage

* `grunt watch` when you're ready to start developing: this will watch for changes to Sass, Javascript, or images, and perform appropriate tasks (compiling Sass, bundling javascript, etc)

* While `grunt server` is running, any browser tabs with your dev site open in will update live (via the magic of [Browsersync](https://www.browsersync.io/)) as you edit styles, templates, etc.

* If this is a fresh install of Craft, you may see error pages until you've installed it by visiting  [http://craft.dev/admin/install](http://craft.dev/admin/install)

* Crafty has a database-provisioning shell script. If you run `vagrant provision --provision-with shell`, the most recent backup in `app\craft\storage\backups` will be restored. (Of course, you'll lose any current state of the database, so only do this when you're happy for that to happen)

* Crafty uses [Browserify](http://browserify.org/)

## Environment

* Ubuntu 14.04 (Trusty Tahr)
* Apache 2.4
* PHP 5 (with xdebug and all Craft's [required PHP extensions](http://buildwithcraft.com/docs/requirements#required-php-extensions))
* [Bourbon](http://bourbon.io/)
* [Neat](http://neat.bourbon.io/)
* [jQuery](http://jquery.com/)
* [Modernizr](http://modernizr.com/)
